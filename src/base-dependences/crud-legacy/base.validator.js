const Validator = require('validatorjs');

module.exports = class BaseValidator extends Validator {
    constructor(data, rules, customMessages) {
        super(data, rules, customMessages);

        BaseValidator.registerAsync('unique', this.uniqueRule, BaseValidator.getMessages(this.lang).unique);
        BaseValidator.registerAsync('exists', this.existsRule, BaseValidator.getMessages(this.lang).exists);
        BaseValidator.registerAsync('not_exists', this.notExistsRule, BaseValidator.getMessages(this.lang).not_exists);
        BaseValidator.registerAsync('block', this.blockRule, BaseValidator.getMessages(this.lang).block);
    }

    getError() {
        let error = new Error(this.constructor.name);
        error.errors = this.errors.all();
        error.status = 400;

        return error;
    }

    async uniqueRule(value, req, key, passes) {
        let params = this.getParameters();
        if(params.length >= 1) {
            let repositoryName = params[0];
            let queryKey = params[1] || key;

            try {
                let Repository = require('../repositories/' + repositoryName + 'Repository');
                let repository = new Repository();
                
                let query = {};
                query[queryKey] = value;
                let items = await repository.search(query);

                return passes(!items.length);
            } catch(error) {
                console.error(error);
                return passes(false);
            }
        }
        
        return passes(false);
    }

    async existsRule(value, req, key, passes) {
        let params = this.getParameters();
        if(params.length >= 1) {
            let repositoryName = params[0];
            let foreignKey = params[1] || 'id';

            try {
                let Repository = require('../repositories/' + repositoryName + 'Repository');
                
                let approved = false;

                if(Array.isArray(value)) {
                    let valid = true;

                    for(let i in value) {
                        let query = {};
                        query[foreignKey] = value[i];
                        
                        let repository = new Repository();
                        let items = await repository.search(query);

                        if(!items.length) {
                            valid = false;
                        }
                    }

                    if(valid) {
                        approved = true;
                    }
                } else {
                    let query = {};
                    query[foreignKey] = value;

                    let repository = new Repository();
                    let items = await repository.search(query);

                    approved = !!items.length;
                }
                
                return passes(approved);
            } catch(error) {
                console.error(error);
                return passes(false);
            }
        }
        
        return passes(false);
    }

    async notExistsRule(value, req, key, passes) {
        let params = this.getParameters();
        if(params.length == 2) {
            let repositoryName = params[0];
            let foreignKey = params[1];

            try {
                let Repository = require('../repositories/' + repositoryName + 'Repository');
                
                let approved = false;

                if(Array.isArray(value)) {
                    let valid = true;

                    for(let i in value) {
                        let query = {};
                        query[foreignKey] = value[i];
                        
                        let repository = new Repository();
                        let items = await repository.search(query);

                        if(items.length) {
                            valid = false;
                        }
                    }

                    if(valid) {
                        approved = true;
                    }
                } else {
                    let query = {};
                    query[foreignKey] = value;
                    
                    let repository = new Repository();
                    let items = await repository.search(query);

                    approved = !items.length;
                }
                
                return passes(approved);
            } catch(error) {
                console.error(error);
                return passes(false);
            }
        }
        
        return passes(false);
    }

    async blockRule(value, req, key, passes) {
        return passes(false);
    }

    async fails() {
        let promise = new Promise(resolve => {
            this.checkAsync(() => {
                resolve(false);
            }, () => {
                resolve(true);
            });
        });
        return promise;
    }
}