const { isValidObjectId } = require("mongoose");
const { ObjectId } = require("mongodb");

module.exports = class BaseRepository {
	locale = (process.env.LANGUAGE || "en_US").split("_")[0];

	constructor(model) {
		this.model = model;
		// this.model.schema.virtual("id").get(function(this) {
		// 	return this._id;
		// }).set(function(this, value) {
		// 	this._id = value;
		// });

		this.isFindOne = false;
		this.relations = {
			lookup: {
				from: "",
				as: "",
				localField: "",
				foreignField: "",
				let: "",
				pipeline: [],
			},
			justOne: false
		};
		this.query = {};
		this.aggregate = this.model.aggregate();
	}
	init() {
		this.isFindOne = false;
		this.query = {};
		this.aggregate = this.model.aggregate();
	}
	all(query) {
		query = this.getFormattedQuery(query, false);
		this.aggregate = this.aggregate.match(query);
		return this;
	}
	find(query) {
		query = this.getFormattedQuery(query);
		this.aggregate = this.aggregate.match(query);
		return this;
	}
	first(query) {
		query = this.getFormattedQuery(query);
		this.aggregate = this.aggregate.match(query);
		this.size(1);
		this.isFindOne = true;
		return this;
	}
	findById(id) {
		const query = this.getFormattedQuery({ id });
		console.log(query)
		this.aggregate = this.aggregate.match(query);
		this.isFindOne = true;
		return this;
	}
	startAt(startAt = 0) {
		this.aggregate = this.aggregate.skip(startAt);
		return this;
	}
	sort(sort = {}) {
		this.aggregate = this.aggregate.collation({ locale: this.locale });
		this.aggregate = this.aggregate.sort(sort);
		return this;
	}
	size(size) {
		this.aggregate = this.aggregate.limit(Number(size));
		return this;
	}

	async countAggregate() {
		this.aggregate = this.aggregate.count("count");
		const result = await this.exec();

		if (result && result.length) {
			return result[0].count ?? 0;
		}
		return 0;
	}

	async count(query) {
		query = this.getFormattedQuery(query);
		return await this.model.find(query).countDocuments() ?? 0;
	}
	async firstOrCreate(props) {

		const first = await this.first(props).exec();
		if (first) {
			return first;
		} else {
			return await this.model.create(props);
		}
	}
	async create(props) {
		return await this.model.create(props);
	}
	async insertMany(props) {
		return await this.model.insertMany(props);
	}
	async updateById(id, props) {
		const query = this.getFormattedQuery({ id }, false);
		return await this.model.findOneAndUpdate(query, props);
	}
	async updateManyById(ids, props) {
		const query = this.getFormattedQuery({ id: ids }, false);
		return await this.model.updateMany(query, props);
	}
	async deleteById(id) {
		const query = this.getFormattedQuery({ id }, false);
		return await this.model.findOneAndUpdate(query, {
			enabled: false
		});
	}

	async permanentDelete(id) {
		const query = this.getFormattedQuery({ id }, false);
		return await this.model.deleteOne(query);
	}

	async clear() {
		await this.model.deleteMany({});
	}

	async deleteMany(query) {
		if (Object.keys(query).length) {
			await this.model.deleteMany(query);
		}
	}

	where(key, value) {
		this.query[key] = value;
		return this;
	}
	with(...relations) {
		relations.forEach(relationName => {
			const relation = this.relations[relationName];
			if (relation) {
				this.aggregate = this.aggregate.lookup(relation.lookup);
				if (relation.justOne) {
					this.aggregate = this.aggregate.unwind({ path: "$" + relation.lookup.as, preserveNullAndEmptyArrays: true });
				}
			}
		});
		return this;
	}
	select(fields) {
		const project = {};
		Object.entries(fields).forEach(([key, value]) => {

			project[key] = value ? (typeof value === "boolean" ? 1 : value) : 0;
		});
		if (Object.keys(project).length) {
			this.aggregate = this.aggregate.project(project);
		}
		return this;
	}
	unwind(unwind) {
		this.aggregate = this.aggregate.unwind(unwind);
		return this;
	}

	group(fields) {
		const project = {};

		Object.entries(fields).forEach(([key, value]) => {
			project[key] = value;
		});

		if (Object.keys(project).length) {
			this.aggregate = this.aggregate.group(project);
		}

		return this;
	}

	getAggregate() {
		return this.aggregate;
	}

	getFormattedQuery(query, validateKey = true) {
		if (this.validateKey && !Object.prototype.hasOwnProperty.call(query, this.validateKey) && validateKey) {
			query[this.validateKey] = true;
		}
		return this.formatQuery(Object.assign(query, this.query));
	}
	formatQuery(query = {}) {
		if (Object.prototype.hasOwnProperty.call(query, "$or")) {
			query.$or = query.$or.map((q) => {
				if (Object.prototype.hasOwnProperty.call(q, "id")) {
					q._id = Array.isArray(q.id) ? q.id.map(ids => typeof ids == "string" ? ids : parseId(ids)) : (typeof q.id == "string" ? q.id : parseId(q.id));
					delete q.id;
				}
				for (const key in q) {
					const value = q[key];

					if (typeof value == "undefined") {
						delete q[key];
						continue;
					}

					if (Array.isArray(value) && key.charAt(0) != "$") {
						q[key] = { $in: value };
					}

					if (typeof value == "object" && value != null) {
						if (Object.prototype.hasOwnProperty.call(value, "lt")) {
							q[key].$lt = q[key].lt;
							delete q[key].lt;
						}

						if (Object.prototype.hasOwnProperty.call(value, "lte")) {
							q[key].$lte = q[key].lte;
							delete q[key].lte;
						}

						if (Object.prototype.hasOwnProperty.call(value, "gte")) {
							q[key].$gte = q[key].gte;
							delete q[key].gte;
						}

						if (Object.prototype.hasOwnProperty.call(value, "gt")) {
							q[key].$gt = q[key].gt;
							delete q[key].gt;
						}
					}
				}

				return q;
			});
		} else {
			if (Object.prototype.hasOwnProperty.call(query, "id")) {
				query._id = Array.isArray(query.id) ? query.id.map(ids => typeof ids == "string" ? ids : parseId(ids)) : (typeof query.id == "string" ? query.id : parseId(query.id));
				delete query.id;
			}
			for (const key in query) {
				const value = query[key];

				if (typeof value == "undefined") {
					delete query[key];
					continue;
				}

				if (Array.isArray(value) && key.charAt(0) != "$") {
					query[key] = { $in: value };
				}

				if (typeof value == "object" && value != null) {
					if (Object.prototype.hasOwnProperty.call(value, "lt")) {
						query[key].$lt = query[key].lt;
						delete query[key].lt;
					}

					if (Object.prototype.hasOwnProperty.call(value, "lte")) {
						query[key].$lte = query[key].lte;
						delete query[key].lte;
					}

					if (Object.prototype.hasOwnProperty.call(value, "gte")) {
						query[key].$gte = query[key].gte;
						delete query[key].gte;
					}

					if (Object.prototype.hasOwnProperty.call(value, "gt")) {
						query[key].$gt = query[key].gt;
						delete query[key].gt;
					}
				}
			}
		}
		return query;
	}

	async exec() {
		let result = await this.aggregate.exec();
		if (result) {
			if (Array.isArray(result)) {
				const newResult = [];
				result.forEach(doc => {
					const item = doc;
					newResult.push({ ...item, id: item._id });
				});
				if (this.isFindOne) {
					const item = newResult[0];
					result = item ? { ...item, id: item._id } : item;
				} else {
					result = newResult;
				}
			} else {
				const item = result;
				result = { ...item, id: item._id };
			}
		}
		this.init();
		return result;
	}
}