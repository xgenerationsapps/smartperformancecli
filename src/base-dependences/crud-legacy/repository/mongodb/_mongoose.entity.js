module.exports = class _MongooseEntity {
    constructor(model) {
        if(!model) {
            throw new Error('Model is not defined');
        }

        this.model = model;
        this.model.createIndexes();
        
        this.query = {};
        this.project = {};
        this.relations = [];
        this.validateKey = 'enabled';
    }
    all(query) {
        query = this.getFormattedQuery(query, false);
        this.model = this.model.find(query, this.project);
        return this;
    }
    count(query) {
        query = this.getFormattedQuery(query);
        this.model = this.model.countDocuments(query);
        return this;
    }
    find(query) {
        query = this.getFormattedQuery(query);
        this.model = this.model.find(query, this.project);
        return this;
    }
    startAt(start_at = 0) {
        this.model = this.model.skip(start_at);
        return this;
    }
    sort(sort) {
        if(sort) {
            this.model = this.model.collation({locale: process.env.LANGUAGE.split('_')[0] }).sort(sort);
        }
        return this;
    }
    size(size) {
        this.model = this.model.limit(size);
        return this;
    }
    create(props) {
        this.model = this.model.create(props);
        return this;
    }
    findById(id) {
        this.model = this.model.findOne({_id: id}, this.project);
        return this;
    }
    updateById(id, props) {
        this.model = this.model.findOneAndUpdate({_id: id}, props);

        return this;
    }
    deleteById(id) {
        this.model = this.model.deleteOne({_id: id});
        return this;
    }
    clear() {
        this.model = this.model.deleteMany({});
        return this;
    }
    where(key, value) {
        this.query[key] = value;
        return this;
    }
    getIds(query) {
        query = this.getFormattedQuery(query);
        this.model = this.model.find(query);
        this.model = this.model.distinct('_id');

        return this;
    }
    with(...relations) {
        this.relations = relations;
        return this;
    }
    select(fields) {
        for(let key in fields) {
            this.project[key] = fields[key] ? 1 : 0;
        }
        return this;
    }

    getFormattedQuery(query = {}, validateKey = true) {
        if(this.validateKey && !query.hasOwnProperty(this.validateKey) && validateKey) {
            query[this.validateKey] = true;
        }
        return this.formatQuery(Object.assign(query, this.query));
    }
    formatQuery(query) {
        if(query.hasOwnProperty('id')) {
            query._id = query.id;
            delete query.id;
        }
        for(let key in query) {
            let value = query[key];

            if(typeof value == 'undefined') {
                delete query[key];
                return;
            }

            if(Array.isArray(value) && key.charAt(0) != '$') {
                query[key] = {$in: value};
            }

            if(typeof value == 'object' && value != null) {
                if(value.hasOwnProperty('lt')) {
                    query[key].$lt = query[key].lt;
                    delete query[key].lt;
                }
    
                if(value.hasOwnProperty('lte')) {
                    query[key].$lte = query[key].lte;
                    delete query[key].lte;
                }
    
                if(value.hasOwnProperty('gte')) {
                    query[key].$gte = query[key].gte;
                    delete query[key].gte;
                }
    
                if(value.hasOwnProperty('gt')) {
                    query[key].$gt = query[key].gt;
                    delete query[key].gt;
                }
            }
        }
        return query;
    }
    async exec() {
        this.relations.forEach(relation => {
            try {
                this.model = this.model.populate(relation);
            } catch {}
        });
        return await this.model;
    }
}