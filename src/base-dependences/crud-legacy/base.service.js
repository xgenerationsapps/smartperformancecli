module.exports = class BaseService {
    constructor(Repository, CreateValidator, UpdateValidator, DeleteValidator) {
        if(!Repository) {
            throw new Error('Repository is not defined');
        }
        this.repository = new Repository();
        this.createValidator = CreateValidator;
        this.updateValidator = UpdateValidator;
        this.deleteValidator = DeleteValidator;
    }
    async getAll(enabled) {
        let data = await this.repository.getAll(enabled);
        return data;
    }
    
    async find(query) {
       return await this.repository.find(query);
    }

    async paginate(currentPage, perPage, sortBy, sort, query) {
        let paginate = await this.repository.paginate(currentPage, perPage, sortBy, sort, query);
        return paginate;
    }
    async search(query) {
        let search = await this.repository.search(query);

        return search;
    }
    async create(props) {
        if(this.createValidator) {
            let validator = new this.createValidator(props);
    
            if(await validator.fails()) {
                let error = new Error(validator.constructor.name);
                error.errors = validator.errors.all();
                error.status = 400;
                throw error;
            }
        }

        let data = await this.repository.create(props);
        return data;
    }
    async findById(id) {
        let data = await this.repository.findById(id);
        return data;
    }
    async updateById(id, props) {
        if(this.updateValidator) {
            let validator = new this.updateValidator({...props, id});
            if(await validator.fails()) {
                let error = new Error(validator.constructor.name);
                error.errors = validator.errors.all();
                error.status = 400;
                console.log(error)
                throw error;
            }
        }

        let data = await this.repository.updateById(id, props);
        return data;
    }
    async deleteById(id) {
        if(this.deleteValidator) {
            let validator = new this.deleteValidator({id});
            if(await validator.fails()) {
                let error = new Error(validator.constructor.name);
                error.errors = validator.errors.all();
                error.status = 400;
                throw error;
            }

            let data = await this.repository.deleteById(id);
            return data;
        }
    }
}