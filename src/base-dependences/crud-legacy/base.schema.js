const BaseSchema = {
    enabled: {
		type: Boolean,
		default: true,
	}
}

const _DefaultSchemaOptions = {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: '_version',
    toJSON: {
        virtuals: true,
        transform: function(doc, ret) {
            delete ret._id;
        },
        versionKey: false
    },
    toObject: {
        virtuals: true,
        versionKey: false
    },
    id: true,
    autoIndex: true,
};

module.exports = {
    BaseSchema,
    _DefaultSchemaOptions
}