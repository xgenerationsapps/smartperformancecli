
import { IBaseModule } from "../../module-backend-node/module-base/base.interface";
import BaseModuleRepository, { id, newId, parseId, IBaseRepositoryModule } from "../../module-backend-node/module-base/base.repository";
import BaseModuleService from "../../module-backend-node/module-base/base.service";
import BaseValidator from "../../module-backend-node/module-base/base.validator";
import { DefaultSchema, DefaultSchemaOptions } from "../../module-backend-node/module-base/repositories/mongoose/default.schema";
import ImageModuleService from "../../module-backend-node/module-core/image-module/image.service";
import UserModuleService from "../../module-backend-node/module-core/user-module/user-module.service";
import { getAttributes } from "../../module-backend-node/module-utils/reference.utils";
import * as CalendarUtils from "../../module-backend-node/module-utils/calendar";
import AccessTokenMiddleware from "../../module-backend-node/middlewares/access-token.middleware";
import { BaseCacheModuleRepository, IBaseModuleCacheRepository } from "../../module-backend-node/module-base/cache/cache.repository";
import BaseCacheModuleService from "../../module-backend-node/module-base/cache/cache.service";
import { QueueServiceExecutableInterface } from "../../module-backend-node/module-base/queue/queue.interface";
import BaseQueueService from "../../module-backend-node/module-base/queue/queue.service";
import { BaseQueueRepository } from "../../module-backend-node/module-base/queue/queue.repository";
import { IUserModuleBase, IUserModule } from "../../module-backend-node/module-core/user-module/user-module.interface"
import { QueueRepositoryInterface, QueueServiceInterface } from "../../module-backend-node/module-base/queue/queue.interface";
import { Validator } from "../../module-backend-node/module-utils/validator.utils";

export const BaseService = BaseModuleService;
export type BaseInterface = IBaseModule;
export const BaseRepository = BaseModuleRepository;
export const ImageService = ImageModuleService
export const UserService = UserModuleService;
export const Calendar = CalendarUtils.default;
export type BaseRepositoryInterface<T> = IBaseRepositoryModule<T>;
export class BaseCacheService<T> extends BaseCacheModuleService<T> { };
export const BaseCacheRepository = BaseCacheModuleRepository;
export type BaseCacheRepositoryInterface<T> = IBaseModuleCacheRepository<T>;
export type UserInterface = IUserModule;

export {
    BaseValidator,
    parseId,
    newId,
    id,
    DefaultSchema,
    DefaultSchemaOptions,
    AccessTokenMiddleware,
    QueueServiceExecutableInterface,
    BaseQueueService,
    BaseQueueRepository,
    Validator,
    QueueRepositoryInterface,
    QueueServiceInterface,
    getAttributes
}
