import { FlexStyle, TextStyle, ViewStyle } from "react-native/types";


export interface ReactProps {
    style?: FlexStyle | FlexStyle[] | ViewStyle | ViewStyle[] | TextStyle | TextStyle[],
    key?: any
}