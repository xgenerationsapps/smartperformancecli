import { RouteProp } from "@react-navigation/native";
import { Param, ParamList } from "../../route";

export interface Navigation {
    goBack: () => any,
    navigate: <T extends Param> (page: T, options?: ParamList[T]) => any,
    push: <T extends Param> (page: T, options?: ParamList[T]) => any,
    replace: <T extends Param> (page: T, options?: ParamList[T]) => any,
    reset: (params: { index: number, routes: [{ name: string }] }) => any,
    pop: (pages: number) => void,
    setOptions: (options: any) => void
}


export interface NavigationProps<T extends object | undefined = undefined> {
    navigation: Navigation,
    route: RouteProp<{ params: T }, 'params'>
}