import { StyleSheet } from "react-native";

export const BaseStyles = StyleSheet.create({
    container: {
        flex: 1
    }
})