import chalk from 'chalk';
import execa from 'execa';
import fs, { exists } from 'fs';
import gitignore from 'gitignore';
import Listr from 'listr';
import ncp from 'ncp';
import path from 'path';
import { promisify } from 'util';
import { getBaseDirectoryConfig } from '../utils/base-directory.utils';
import { capitalize } from '../utils/captalize';
import { pluralize } from '../utils/pluralize';
import { searchFileOrDirectory } from '../utils/search-file-or-directory';

const access = promisify(fs.access);
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const readDir = promisify(fs.readdir);
const mkdir = promisify(fs.mkdir);
const fileExists = promisify(fs.exists);
const dirExists = promisify(fs.exists);
const stat = promisify(fs.stat);
let _baseDirectoryExists = false;

function getInstancePath(platform, type,) {

  const dir = __dirname;

  if (type) {
    return path.resolve(
      dir,
      '../templates',
      platform,
      type
    );
  } else {
    return path.resolve(
      dir,
      '../templates',
      platform
    );
  }

}

function scanTemplatesFiles() {

}


const checkRootExists = async (options) => {


  const root = await dirExists(options.targetDirectory);


  return root;
}

async function copyTemplateFiles(options, logCallback) {

  const root = await checkRootExists(options);

  if (!root) {
    await mkdir(options.targetDirectory, { recursive: true });
  }

  const files = await readDir(options.copyDirectory);

  let smartCliConfig = {
    application: "node",
    useBase: true
  };

  await Promise.all(files.map(async (file) => {

    const fullPath = path.resolve(options.copyDirectory, file);
    const targetDirectory = path.resolve(options.targetDirectory);

    // console.log("targetDirectory", targetDirectory);

    let baseSearched = false;
    let baseDirectory = "";
    let baseDirectoryFullPath = "";
    let baseTag = "base";

    const _smartCliConfig = await searchFileOrDirectory({
      searchInDirectory: path.resolve(new String(targetDirectory).toString()),
      type: "file",
      includedFiles: [".smart-cli.config.json"],
      ignoredFiles: [],
    })

    if (_smartCliConfig && _smartCliConfig.data) {
      smartCliConfig = _smartCliConfig.data;
    }

    if (smartCliConfig.useBase) {

      const nodeAppFilesInDirectory = await searchFileOrDirectory({
        searchInDirectory: path.resolve(new String(targetDirectory).toString()),
        type: "directory",
        includedFiles: ["base", "Base"],
        ignoredFiles: [".baseignore"],
      });

      const { relativePath, fullPath, fileSearched, endPointTag } = nodeAppFilesInDirectory;

      baseSearched = fileSearched;
      baseDirectory = relativePath;
      baseDirectoryFullPath = fullPath;
      baseTag = endPointTag;
      _baseDirectoryExists = baseSearched;

      if (baseSearched) {
        baseDirectory += baseTag;
      } else {
        baseDirectory = baseTag
      }

    }

    switch (smartCliConfig.application) {
      case "java":

        const pathAfterJava = targetDirectory.replace(/\\/g, ".").split(".java.")[0];
        const pathBeforeJava = targetDirectory.replace(/\\/g, ".").split(".java.")[1];

        // console.log("pathAfterJava", pathAfterJava, "pathBeforeJava", pathBeforeJava);
        // console.log("baseDirectoryFullPath", baseDirectoryFullPath,);

        smartCliConfig.corePackageTarget = targetDirectory.replace(/\\/g, ".").split(".java.")[1]

        const baseDirectoryFullPathSplit = baseDirectoryFullPath.replace(/\\/g, ".").split(".java.")[1].split(".");

        smartCliConfig.basePackageTarget = baseDirectoryFullPathSplit.reduce((current, next) => {
          const pathIncluded = pathBeforeJava.split(".").find((path) => path.includes(next));

          if (pathIncluded) {
            return current += "." + pathIncluded;
          }
          return current += "";
        }, "");

        smartCliConfig.basePackageTarget = String(smartCliConfig.basePackageTarget).substring(1, smartCliConfig.basePackageTarget.length);

        // console.log("smartCliConfig", smartCliConfig.basePackageTarget);

        break;
      case "node":
        break;

    }

    // console.log("base fullPath", baseDirectoryFullPath);

    // console.log("subDirectories:", path.resolve(baseDirectory,baseTag), fullPath);

    const newCopyFileName = options.name.toLowerCase();

    const newTemplatesPOOClassName = options.name;
    const newTemplatesFunctionName = newCopyFileName.split(/-/g).map(subName => capitalize(subName)).join('');
    const newTemplatesResourceName = newCopyFileName.split(/-/g)[0] + newCopyFileName.split(/-/g).slice(1).map(subName => capitalize(subName)).join('');
    const newTemplatesFunctionNamePlural = pluralize(newTemplatesFunctionName);
    const newTemplatesResourceNamePlural = pluralize(newTemplatesResourceName);
    const newTemplateUnderlineName = newCopyFileName.replace(/-/g, "_");

    const statResponse = await stat(path.resolve(options.copyDirectory, file));

    if (statResponse.isDirectory()) {

      const newOptions = {
        root: options.root,
        targetDirectory: path.resolve(options.targetDirectory, file),
        copyDirectory: path.resolve(options.copyDirectory, file),
        baseDependencesDirectory: options.baseDependencesDirectory,
        name: options.name,
        isBaseDependence: options.isBaseDependence
      }

      await copyTemplateFiles(newOptions, logCallback);


    } else {

      if (file.includes(".base-dependences.json")) {

        const baseDependenceFileData = await readFile(fullPath, 'utf8');

        const targetDirectory = path.resolve(path.resolve(options.targetDirectory), baseDirectory);

        for (let data of JSON.parse(baseDependenceFileData)) {
          const { dir } = data;

          const newOptions = {
            root: options.root,
            targetDirectory: path.resolve(targetDirectory),
            copyDirectory: path.resolve(options.baseDependencesDirectory, dir),
            baseDependencesDirectory: options.baseDependencesDirectory,
            name: options.name,
            isBaseDependence: true
          }

          await copyTemplateFiles(newOptions, logCallback);
        }
        return;
      }


      if (options.isBaseDependence) {

        if (await checkRootExists({
          targetDirectory: path.resolve(options.targetDirectory, file.replace('_name', newCopyFileName))
        })) {
          console.log(`%s Base Dependency ignored as it already exists. ${file}`, chalk.yellow.bold("⚠"));
          return;
        }

      }

      const data = await readFile(fullPath, 'utf8');

      let newData = data;

      switch (smartCliConfig.application) {
        case "node":
          newData = newData.replace(/_namespace/g, newCopyFileName);
          newData = newData.replace(/_Names/g, newTemplatesFunctionNamePlural);
          newData = newData.replace(/_names/g, newTemplatesResourceNamePlural);
          newData = newData.replace(/_Name/g, newTemplatesFunctionName);
          newData = newData.replace(/_nameService/g, newTemplatesResourceName + "Service")
          newData = newData.replace(/_nameValidator/g, newTemplatesResourceName + "Validator");
          newData = newData.replace(/_nameController/g, newTemplatesResourceName + "Controller");
          newData = newData.replace(/_nameCallMethod/g, newTemplatesResourceName + "CallMethod");
          newData = newData.replace(/_namePath/g, newTemplatesResourceName);
          newData = newData.replace(/_name/g, newCopyFileName);
          newData = newData.replace(/_baseDirectory/g, baseDirectory);
          newData = newData.replace(/_UnderName/g, newTemplateUnderlineName);
          newData = newData.replace(/_UnderSchemaName/g, pluralize(newTemplateUnderlineName));
          break;
        case "java":
          newData = newData.replace(/_CorePackageName/g, smartCliConfig.corePackageTarget)
          newData = newData.replace(/_BasePackageName/g, smartCliConfig.basePackageTarget)
          newData = newData.replace(/_Name/g, newTemplatesPOOClassName);
          newData = newData.replace(/_name/g, newCopyFileName);
          newData = newData.replace(/_BaseTag/g, baseTag);
          break;
      }

      let errorWrite = undefined;
      let newFileName = "";
      if (file.includes('_name')) {
        newFileName = file.replace('_name', newCopyFileName);
        errorWrite = await writeFile(path.resolve(options.targetDirectory, newFileName), newData);
      } else if (file.includes('_Name')) {
        newFileName = file.replace('_Name', newTemplatesPOOClassName);
        errorWrite = await writeFile(path.resolve(options.targetDirectory, newFileName), newData);
      }

      if (!errorWrite) {
        logCallback({
          title: "file created!",
          name: newFileName
        })
        //return console.log('%s file created!', chalk.bold(file.replace('_name', newCopyFileName)));
      }

    }

  }))


}

export async function createProject(options) {

  options = {
    ...options,
  };


  if (options.template.includes("@types/")) {
    console.log(options.template);

    options.copyDirectory = getInstancePath(options.template);
  } else {
    options.copyDirectory = getInstancePath(options.platform, options.template);
  }
  options.root = path.resolve(process.cwd());
  options.targetDirectory = path.resolve(options.root, options.directory);
  options.baseDependencesDirectory = path.resolve(__dirname, "base-dependences")


  console.log(getInstancePath(options.template));

  if ((await checkRootExists(options))) {
    console.error(`%s Um diretório com o mesmo nome já existe. ( ${options.name.toLowerCase()} )`, chalk.red.bold("ERROR"))
    return;
  }

  try {
    await access(options.copyDirectory, fs.constants.R_OK);
  } catch (err) {
    console.error('%s Invalid template name', chalk.red.bold('ERROR'));
    process.exit(1);
  }

  const dataCreated = []

  const tasks = new Listr(
    [
      {
        title: 'Gerando templates',
        task: () => copyTemplateFiles(options, (logData) => {
          dataCreated.push({
            title: logData.title,
            name: logData.name
          })
        }),
      }
    ],
    {
      exitOnError: false,
    }
  );

  await tasks.run();


  console.log('%s Templates criados com sucesso!', chalk.green.bold('CONCLUÍDO'));
  for (let log of dataCreated) {
    console.log(`%s ${log.title}`, chalk.bold(log.name))
  }

  if (!_baseDirectoryExists) {
    console.error("%s Não foi encontrado um diretório ( base ) em seu projeto.", chalk.red.bold("ERROR"))
  }

  return true;
}
