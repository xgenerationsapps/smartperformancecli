import { Request, Response } from "express";
import _NameService from "./_name.service";

export const index = async (req: Request, res: Response) => {
	const _nameService = new _NameService();
	
	const data = await _nameService.index();
	return res.send([]);
};
