import { Router } from "express";
import * as _NameController from "./_name.controller";

const _NameRoutes = Router();

_NameRoutes.get("/", _NameController.index);

export default _NameRoutes;