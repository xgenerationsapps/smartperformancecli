import { BaseInterface } from "_baseDirectory/external-references";

export interface _NameInterface extends BaseInterface {
    name: string
}