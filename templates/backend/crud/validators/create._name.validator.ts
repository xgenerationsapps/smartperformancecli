import { BaseValidator } from "_baseDirectory/external-references";
import { _NameInterface } from "../_name.interface";

export default class Create_NameValidator extends BaseValidator<_NameInterface> {
	constructor(data: any) {
		super(data, {
			name: ["required", "unique:_namespace"],
		});
	}
}