import { BaseValidator } from "_baseDirectory/external-references";
import { _NameInterface } from "../_name.interface";

export default class Delete_NameValidator extends BaseValidator<_NameInterface> {
	constructor(data: any) {
		super(data, {
			id: ["required", "exists:_namespace"],
		});
	}
}