import { Schema } from "mongoose";
import {DefaultSchema, DefaultSchemaOptions} from "_baseDirectory/external-references";

const _NameSchema = new Schema({
	...DefaultSchema,
	name: {
		type: String,
		required: true,
	}
}, DefaultSchemaOptions);

export default _NameSchema;