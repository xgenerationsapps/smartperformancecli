
class _NameService {


    repository = globalThis.db.collection("_UnderSchemaName");
    constructor() {

    }

    async index() {

        return this.repository.find().toArray();
    }

    async create(props) {
        await this.repository.insertOne(props);
    }

    async findById(id) {
        const data = this.repository.find({
            _id: id
        }).toArray();

        return data;
    }

    async updateById(id, props) {
        this.repository.updateOne({ _id: id }, { $set: props })
    }

    async deleteById(id) {
        this.repository.deleteOne({ _id: id })
    }
}

module.exports = _NameService;