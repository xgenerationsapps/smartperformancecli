const BaseService = require("_baseDirectory/base.service");
const _NameRepository = require("./_name.repository");
const Create_NameValidator = require("./validators/create._name.validator");
const Update_NameValidator = require("./validators/update._name.validator");
const Delete_NameValidator = require("./validators/delete._name.validator");

class _NameService extends BaseService {

    repository = new _NameRepository();
    constructor() {
        super(
            _NameRepository,
            Create_NameValidator,
            Update_NameValidator,
            Delete_NameValidator
        )

    }

    async index() {

        return this.repository.find().exec();
    }

}

module.exports = _NameService;