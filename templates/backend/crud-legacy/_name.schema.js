const { model, Schema } = require("mongoose");
const { BaseSchema, _DefaultSchemaOptions } = require("_baseDirectory/base.schema");

const _NameSchema = new Schema({
    ...BaseSchema
}, _DefaultSchemaOptions)

module.exports = _NameSchema;

