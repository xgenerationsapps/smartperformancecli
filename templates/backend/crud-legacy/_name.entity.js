const BaseEntity = require("_baseDirectory/base.entity");
const _NameSchema = require("./_name.model");

module.exports = class _NameEntity extends BaseEntity{
    constructor(){
        super(_NameSchema)
    }
}