const BaseValidator = require("_baseDirectory/base.validator")

module.exports = class Update_NameValidator extends BaseValidator {
    constructor(data) {
        super(data, {
            id: ["required"]
        })
    }
}