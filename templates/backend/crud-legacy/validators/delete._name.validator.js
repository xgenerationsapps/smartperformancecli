const BaseValidator = require("_baseDirectory/base.validator")

module.exports = class Delete_NameValidator extends BaseValidator {
    constructor(data) {
        super(data, {
            id: ["required"]
        })
    }
}