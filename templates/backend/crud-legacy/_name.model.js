const { model, Schema } = require("mongoose");
const { BaseSchema, _DefaultSchemaOptions } = require("_baseDirectory/base.schema");
const _NameSchema = require("./_name.schema");

module.exports = model('_Name', _NameSchema, '_UnderSchemaName');