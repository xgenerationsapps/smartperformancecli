const BaseRepository = require("_baseDirectory/base.repository");
const _NameModel = require("./_name.model");

module.exports = class _NameRepository extends BaseRepository{
    constructor(){
        super(_NameModel)
    }
}