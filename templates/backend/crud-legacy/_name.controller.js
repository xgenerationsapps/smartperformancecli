const _NameService = require("./_name.service")

const getInstanceService = () => {
    return new _NameService();
}

const index = async (req, res) => {
    const _nameService = getInstanceService();

    const data = await _nameService.index();

    res.send(data)
}


const create = async (req, res) => {
    const _nameService = getInstanceService();

    const props = req.body;
    const data = await _nameService.create(props);

    return res.send(data);
};

const findById = async (req, res) => {
    const _nameService = getInstanceService();

    const { id } = req.params;
    const data = await _nameService.findById(id);

    return res.send(data);
};

const updateById = async (req, res) => {
    const _nameService = getInstanceService();

    const props = req.body;
    const { id } = req.params;
    const data = await _nameService.updateById(id, props);

    return res.send(data);
};

const deleteById = async (req, res) => {
    const _nameService = getInstanceService();

    const { id } = req.params;
    const data = await _nameService.deleteById(id);

    return res.send(data);
};

module.exports = {
    index,
    create,
    findById,
    updateById,
    deleteById
}
