import { Request, Response } from "express";
import Management_NameService from "./management-_name.service";
import * as SearchUtils from "../../../utils/custom-search.utils";
import { UserService } from "_baseDirectory/external-references";
import { CustomSearch } from "_baseDirectory/types/custom-search.types";
import Management_NameCacheService from "./cache/management-_name-cache.service";
import Management_NameCacheRepository from "./cache/management-_name-cache.repository";

const getInstance = () => {

    return new Management_NameService();
}

export const index = async (req: Request, res: Response) => {

    const service = getInstance();

    let response = await service.index();

    res.send(response);

}

export const create = async (req: Request, res: Response) => {

    const service = getInstance();

    const { name, description } = req.body;

    const _personal_info = {

        name: "Usuário",
        last_name: "Teste"

    }

    let response: any = {};

    let searchModel = SearchUtils.getSearchModel({ name, description });

    searchModel.creator.displayName = _personal_info.name + " " + _personal_info.last_name;

    const created = await service.create(searchModel);

    searchModel.type = "_name";

    response = SearchUtils.getDefaultKeys(searchModel);

    response._id = created.id;

    res.send(response);

}

export const save = async (req: Request, res: Response) => {

}

export const read = async (req: Request, res: Response) => {

    const { search_id } = req.body;

    if (!search_id) {
        return res.send({});
    }

    const service = getInstance();

    let response = await service.read(search_id);

    res.send(response);

}

export const readFilters = async (req: Request, res: Response) => {

    const data = await getInstance().readFilters();

    res.send(data);

}


export const publish = async (req: Request, res: Response) => {

    const service = getInstance();
    const executionRoutineCacheService = new Management_NameCacheService(
        new Management_NameCacheRepository()
    );

    const searchModel = req.body as CustomSearch;

    const userService = new UserService();
    const { id } = res.locals["user"];
    const user = await userService.getMe(id);

    searchModel.creator = {
        id,
        displayName: user?.name ?? ""
    }


    let response = await service.publish(searchModel);
    await executionRoutineCacheService.clear();
    
    console.log("clear cache _name")
    res.send(response);

}