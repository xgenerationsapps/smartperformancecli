
import Management_NameRepository from "./management-_name.repository";
import CreateManagement_NameValidator from "./validators/create.management-_name.validator";
import DeleteManagement_NameValidator from "./validators/delete.management-_name.validator";
import UpdateManagement_NameValidator from "./validators/update.management-_name.validator";
import * as SearchUtils from "../../../utils/custom-search.utils";
import DraftManagement_NameService from "../draft/draft-_name.service";
import CustomSearchService from "_baseDirectory/services/custom-search.service";
import { parseId } from "_baseDirectory/external-references";


const getDraftInstance = () => {
    return new DraftManagement_NameService();
}


export default class Management_NameService extends CustomSearchService {
    constructor() {
        super(Management_NameRepository, CreateManagement_NameValidator, UpdateManagement_NameValidator, DeleteManagement_NameValidator);
    }

    async index() {

        const executionRoutineDraftService = getDraftInstance();

        let response: { search_list: any[] } = {
            search_list: []
        }

        const searchList = <any>await this.repository.find()
            .select({
                _id: 1,
                name: 1,
                creator: 1,
                description: 1,
                updatedAt: 1,
                status: 1,
                published: 1
            })
            .exec();


        await Promise.all(searchList.map(async (custom_search: any) => {

            let id = custom_search.id.toString();

            //Verificar se a pesquisa existe nos rascunhos
            let search_draft = await executionRoutineDraftService.search({ customSearchId: id });

            if (Object.keys(search_draft).length > 0) {

                custom_search = search_draft[0];

                custom_search.status = 0;
                custom_search._id = id;
            }


            let search_model = SearchUtils.getListSearchModel(custom_search);

            custom_search = Object.assign(search_model, SearchUtils.getDefaultKeys(custom_search));

            if (custom_search) {

                response.search_list.push(custom_search);

            }
        }))

        return response;
    }

    async read(search_id: string) {

        let response: any = {};
        const executionRoutineDraftService = getDraftInstance();

        let spot_search_data = undefined;

        //Verificar se a pesquisa existe nos rascunhos
        spot_search_data = await executionRoutineDraftService.search({ customSearchId: parseId(search_id) });

        if (!spot_search_data.length)
            spot_search_data = await this.search({ id: search_id });

        if (spot_search_data.length > 0) {

            response = spot_search_data[0];

            let search_filters = response.filters;

            if (!Array.isArray(search_filters)) {
                let new_search_filters = [];

                for (let key in search_filters) {
                    new_search_filters.push({
                        name: key,
                        filter_list: search_filters[key]
                    })
                }

                response.filters = new_search_filters;
            }

        }

        response.id = search_id;
        response._id = search_id;

        return response;

    }

}