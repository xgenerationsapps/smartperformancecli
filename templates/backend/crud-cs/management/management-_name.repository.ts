import { BaseRepository } from "_baseDirectory/external-references";
import Management_NameModel from "./management-_name.model";

export default class Management_NameRepository extends BaseRepository<any> {
	constructor() {
		super(Management_NameModel);
	}
}