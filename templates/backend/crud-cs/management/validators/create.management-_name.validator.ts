import { BaseValidator } from "_baseDirectory/external-references";

export default class CreateManagement_NameValidator extends BaseValidator<any> {
    constructor(data: any) {
        super(data, {
            name: ["required", "string"],
            description: ["required", "string"]
        });
    }
}