import { Schema, model } from "mongoose";
import { DefaultSchema, DefaultSchemaOptions } from "_baseDirectory/external-references";
import { CustomSearchSchema } from "_baseDirectory/schemas/custom-search.schema";

const Management_NameSchema = new Schema({
    ...DefaultSchema,
    ...CustomSearchSchema

}, DefaultSchemaOptions)


const Management_NameModel = model("Management_Name", Management_NameSchema);

export default Management_NameModel;