import { BaseCacheRepository } from "_baseDirectory/external-references";
import { Management_NameCacheInterface, Management_NameCacheRepositoryInterface } from "./_name-cache.interface";

export default class Management_NameCacheRepository 
extends BaseCacheRepository<Management_NameCacheInterface>
implements Management_NameCacheRepositoryInterface {
   constructor(){
    super("_name");
   };
};