import { BaseCacheRepositoryInterface, BaseCacheService } from "_baseDirectory/external-references";

export interface Management_NameCacheInterface{
    search: string
}

export interface Management_NameCacheRepositoryInterface extends BaseCacheRepositoryInterface<Management_NameCacheInterface> {};

export interface Management_NameCacheServiceInterface extends BaseCacheService<Management_NameCacheInterface> {};