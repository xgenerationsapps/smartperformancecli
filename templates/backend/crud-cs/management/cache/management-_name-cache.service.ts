import { BaseCacheService } from "_baseDirectory/external-references";
import { Management_NameCacheInterface, Management_NameCacheRepositoryInterface, Management_NameCacheServiceInterface } from "./_name-cache.interface";

export default class Management_NameCacheService extends BaseCacheService<Management_NameCacheInterface>
implements Management_NameCacheServiceInterface {
    constructor(executionRoutineCacheRepository: Management_NameCacheRepositoryInterface){
        super(executionRoutineCacheRepository);
    };
};