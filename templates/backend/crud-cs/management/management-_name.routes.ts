import { Router } from "express";
import Draft_NameRoutes from "../draft/draft-_name.routes";
import * as Management_NameController from "./management-_name.controller";

const Management_NameRoutes = Router();

Management_NameRoutes.use("/draft", Draft_NameRoutes);

Management_NameRoutes.get("/index", Management_NameController.index);
Management_NameRoutes.post("/create", Management_NameController.create);
Management_NameRoutes.post("/read", Management_NameController.read);
Management_NameRoutes.get("/read/filters", Management_NameController.readFilters);
Management_NameRoutes.post("/publish", Management_NameController.publish);

export default Management_NameRoutes;