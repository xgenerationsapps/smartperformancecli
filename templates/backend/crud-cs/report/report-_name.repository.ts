import { Report_NameInterface } from "./report-_name.interface";
import { BaseRepository } from "_baseDirectory/external-references";
import Report_NameModel from "./report-_name.model";

export default class Report_NameRepository extends BaseRepository<Report_NameInterface> {
	constructor() {
		super(Report_NameModel);
	}
}