import { model } from "mongoose";
import Report_NameSchema from "./report-_name.schema";

const Report_NameModel = model("_NameReport", Report_NameSchema);

export default Report_NameModel;