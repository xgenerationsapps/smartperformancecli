import { BaseValidator } from "_baseDirectory/external-references";
import { Report_NameInterface } from "../report-_name.interface";

export default class CreateReport_NameValidator extends BaseValidator<Report_NameInterface> {
	constructor(data: any) {

		super(data, {
		});
	}
}