import { BaseValidator } from "_baseDirectory/external-references";
import { Report_NameInterface } from "../report-_name.interface";

export default class UpdateReport_NameValidator extends BaseValidator<Report_NameInterface> {
	constructor(data: any) {
		super(data, {
			name: ["unique:report_NameValidator"],
		});
	}
}