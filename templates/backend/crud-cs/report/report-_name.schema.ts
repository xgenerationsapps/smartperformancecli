import { Schema } from "mongoose";
import { DefaultSchema, DefaultSchemaOptions } from "_baseDirectory/external-references";
import { ReportSchema } from "_baseDirectory/schemas/report.schema";

const Report_NameSchema = new Schema({ ...DefaultSchema, ...ReportSchema }, DefaultSchemaOptions);


export default Report_NameSchema;