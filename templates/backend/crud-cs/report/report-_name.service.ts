import Report_NameRepository from "./report-_name.repository";
import CreateReport_NameValidator from "./validators/create.report-_name.validator";
import DeleteReport_NameValidator from "./validators/delete.report-_name.validator";
import UpdateReport_NameValidator from "./validators/update.report-_name.validator";
import CustomSearchReportService from "_baseDirectory/services/custom-search-report.service";

export default class Report_NameService extends CustomSearchReportService {
	constructor() {
		super(Report_NameRepository, CreateReport_NameValidator, UpdateReport_NameValidator, DeleteReport_NameValidator);
	}
}