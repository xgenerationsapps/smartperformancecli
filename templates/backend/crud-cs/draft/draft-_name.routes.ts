import { Router } from "express";
import * as Draft_NameController from "./draft-_name.controller";

const Draft_NameRoutes = Router();

Draft_NameRoutes.post("/create", Draft_NameController.create);


export default Draft_NameRoutes;