import Draft_NameRepository from "./draft-_name.repository";
import CustomSearchService from "_baseDirectory/services/custom-search.service";
import CreateDraft_NameValidator from './validators/create.draft-_name.validator';
import UpdateSpotSearchValidator from './validators/update.draft-_name.validator';
import DeleteDraft_NameValidator from './validators/delete.draft-_name.validator';

export default class Draft_NameService extends CustomSearchService {
    constructor() {
        super(Draft_NameRepository, CreateDraft_NameValidator, UpdateSpotSearchValidator, DeleteDraft_NameValidator)
    }

}