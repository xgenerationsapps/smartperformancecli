import { Request, Response } from "express";
import Draft_NameService from "./draft-_name.service";

const getInstance = () => {

    return new Draft_NameService();
}

export const create = async (req: Request, res: Response) => {

    const service = getInstance();

    const searchModel = req.body;

    searchModel.type = "_name";

    let response = await service.saveDraft(searchModel);

    res.send(response);

}

