import { BaseInterface } from "_baseDirectory/external-references";
import { CustomSearchReportType } from "_baseDirectory/types/custom-search-report";

export interface Draft_NameInterface extends BaseInterface, CustomSearchReportType {
    name: string,
}