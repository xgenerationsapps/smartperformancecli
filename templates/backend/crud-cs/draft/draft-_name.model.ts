import { Schema, model } from "mongoose";
import { DefaultSchema, DefaultSchemaOptions } from "_baseDirectory/external-references";
import { CustomSearchSchema } from "_baseDirectory/schemas/custom-search.schema";


const Draft_NameSchema = new Schema({
    ...DefaultSchema,
    ...CustomSearchSchema

}, { ...DefaultSchemaOptions, autoIndex: false },

)

const Draft_NameModel = model("Draft_NameDraft", Draft_NameSchema);

export default Draft_NameModel;