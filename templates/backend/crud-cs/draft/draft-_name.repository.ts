import { BaseRepository } from "_baseDirectory/external-references"
import Draft_NameModel from "./draft-_name.model"

export default class Draft_NameRepository extends BaseRepository<any>{
    constructor(){
        super(Draft_NameModel)
    }
}