import { BaseValidator } from "_baseDirectory/external-references";
import { Draft_NameInterface } from "../draft-_name.interface";

export default class UpdateDraft_NameValidator extends BaseValidator<Draft_NameInterface> {
	constructor(data: any) {
		super(data, {
			name: ["required","string"],
			description: ["required","string"],
		});
	}
}