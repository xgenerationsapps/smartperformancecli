import { BaseValidator } from "_baseDirectory/external-references";
import { Draft_NameInterface } from "../draft-_name.interface";

export default class DeleteDraft_NameValidator extends BaseValidator<Draft_NameInterface> {
	constructor(data: any) {
		super(data, {
			id: ["required", "exists:report_NameValidator"],
		});
	}
}