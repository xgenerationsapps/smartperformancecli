import { QueueServiceExecutableInterface } from "_baseDirectory/external-references";
import { CustomSearchReportType } from "_baseDirectory/types/custom-search-report";
import ReportDistribution_NameService from "../report/report-_name.service";

export default class Distribution_NameSaveReport implements QueueServiceExecutableInterface<CustomSearchReportType>{
    async exec(props: CustomSearchReportType){
		const reportService = new ReportDistribution_NameService();
        await reportService.sendReport({...props});
        return props;
    }
}