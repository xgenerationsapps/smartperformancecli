import { BaseRepository } from "_baseDirectory/external-references";
import Distribution_NameModel from "./distribution-_name.model";

export default class Distribution_NameDistributionRepository extends BaseRepository<any> {
	constructor() {
		super(Distribution_NameModel);
	}
}