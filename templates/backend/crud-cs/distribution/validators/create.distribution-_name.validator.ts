import { BaseValidator } from "_baseDirectory/external-references";

export default class CreateDistribution_NameValidator extends BaseValidator<any> {
    constructor(data: any) {
        super(data, {
            //TODO: Definir regras para criação
            name: ["required", "string"],
            description: ["required", "string"]
        });
    }
}