import { BaseValidator } from "_baseDirectory/external-references";

export default class DeleteDistribution_NameValidator extends BaseValidator<any> {
    constructor(data: any) {
        super(data, {
            id: ['required', 'exists:executionRoutine'],
            //TODO: Definir regras para exclusão
        });
    }
}