import {BaseQueueService} from "_baseDirectory/external-references";
import { PrepareAndSendPictureProps } from "_baseDirectory/services/custom-search.service";
import Distribution_NameService from "../../distribution-_name.service";
import { Distribution_NamePictureRepositoryInterface, Distribution_NamePictureServiceInterface } from "./_name-picture-queue.interface";

export default class Distribution_NamePictureQueueService
extends BaseQueueService<PrepareAndSendPictureProps>
implements Distribution_NamePictureServiceInterface{
    constructor(
        executionRoutinePictureRepositoryInterface: Distribution_NamePictureRepositoryInterface,
        distributionDistribution_NameService: Distribution_NameService
    ){
        super(executionRoutinePictureRepositoryInterface, distributionDistribution_NameService)
    }
}