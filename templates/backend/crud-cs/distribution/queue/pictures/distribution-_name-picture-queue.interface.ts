import { QueueRepositoryInterface } from "_baseDirectory/external-references";
import {BaseQueueService} from "_baseDirectory/external-references";
import { PrepareAndSendPictureProps } from "_baseDirectory/services/custom-search.service";

export interface Distribution_NamePictureRepositoryInterface extends QueueRepositoryInterface<PrepareAndSendPictureProps> {}
export interface Distribution_NamePictureServiceInterface extends BaseQueueService<PrepareAndSendPictureProps> {};