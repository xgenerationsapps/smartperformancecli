import { BaseQueueRepository } from "_baseDirectory/external-references";
import { PrepareAndSendPictureProps } from "_baseDirectory/services/custom-search.service";
import { Distribution_NamePictureRepositoryInterface } from "./distribution-_name-picture-queue.interface";

export default class Distribution_NamePictureQueueRepository
extends BaseQueueRepository<PrepareAndSendPictureProps> 
implements Distribution_NamePictureRepositoryInterface{
    constructor(){
        super("_name-picture");
    };
};