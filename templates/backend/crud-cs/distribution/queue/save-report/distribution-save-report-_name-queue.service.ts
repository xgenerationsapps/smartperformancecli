import {BaseQueueService} from "_baseDirectory/external-references";
import Distribution_NameSaveReport from "../../distribution-_name-save-report.service";
import { CustomSearchReportType } from "_baseDirectory/types/custom-search-report";
import { SaveReportDistribution_NameQueuePictureServiceInterface, SaveReportDistribution_NameQueueRepositoryInterface } from "./save-report-_name-queue.interface";

export default class SaveReportDistribution_NameQueueService
extends BaseQueueService<CustomSearchReportType>
implements SaveReportDistribution_NameQueuePictureServiceInterface{
    constructor(
        saveReportDistribution_NameRepository: SaveReportDistribution_NameQueueRepositoryInterface,
        executionRoutineSaveReport: Distribution_NameSaveReport
    ) {
        super(saveReportDistribution_NameRepository, executionRoutineSaveReport);
    }
}