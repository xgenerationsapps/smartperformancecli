import { QueueRepositoryInterface } from "_baseDirectory/external-references";
import {BaseQueueService} from "_baseDirectory/external-references";
import { CustomSearchReportType } from "_baseDirectory/types/custom-search-report";

export interface SaveReportDistribution_NameQueueRepositoryInterface 
extends QueueRepositoryInterface<CustomSearchReportType>{};

export interface SaveReportDistribution_NameQueuePictureServiceInterface 
extends BaseQueueService<CustomSearchReportType> {};