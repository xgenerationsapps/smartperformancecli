import { BaseQueueRepository } from "_baseDirectory/external-references";
import { CustomSearchReportType } from "_baseDirectory/types/custom-search-report";
import { SaveReportDistribution_NameQueueRepositoryInterface } from "./distribution-save-report-_name-queue.interface";

export default class SaveReportDistribution_NameQueueRepository
extends BaseQueueRepository<CustomSearchReportType>
implements SaveReportDistribution_NameQueueRepositoryInterface{
    constructor(){
        super("save-picture-_name")
    }
}