import { Router } from "express";
import multer from "multer";
import { AccessTokenMiddleware } from "_baseDirectory/external-references";
import Draft_NameRoutes from "../draft/draft-_name.routes";
import * as Distribution_NameController from "./distribution-_name.controller";

const multerUpload = multer({ storage: multer.memoryStorage() });

const Distribution_NameRoutes = Router();

Distribution_NameRoutes.use("/draft",AccessTokenMiddleware, Draft_NameRoutes);

Distribution_NameRoutes.get("/index", Distribution_NameController.index);
Distribution_NameRoutes.post("/create", Distribution_NameController.create);
Distribution_NameRoutes.post("/read", Distribution_NameController.read);
Distribution_NameRoutes.get("/read/filters", Distribution_NameController.readFilters);
Distribution_NameRoutes.get("/read/custom-search-form/:id", Distribution_NameController.readCustomSearchForm);
Distribution_NameRoutes.get("/read/custom-search-report/:id", Distribution_NameController.readCustomSearchReport);
Distribution_NameRoutes.post("/publish", AccessTokenMiddleware, Distribution_NameController.publish);
Distribution_NameRoutes.post("/picture/v2", AccessTokenMiddleware, multerUpload.single("file"), Distribution_NameController.sendPictureV2);
Distribution_NameRoutes.post("/picture", AccessTokenMiddleware, multerUpload.single("file"), Distribution_NameController.sendPictureV2);
Distribution_NameRoutes.post("/put-new-picture", AccessTokenMiddleware, Distribution_NameController.putNewPicture);


export default Distribution_NameRoutes;