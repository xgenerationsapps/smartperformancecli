import Distribution_NameDistributionRepository from "./distribution-_name.repository";
import CreateDistribution_NameValidator from "./validators/create.distribution-_name.validator";
import DeleteDistribution_NameValidator from "./validators/delete.distribution-_name.validator";
import UpdateDistribution_NameValidator from "./validators/update.distribution-_name.validator";

import {
	CustomSearchConfigListInterface
} from "../../configs/custom-search-config-list/custom-search-config-list.interface";
import CustomSearchService, { PrepareAndSendPictureProps } from "_baseDirectory/services/custom-search.service";
import { TypeConfigService } from "_baseDirectory/services/type-config.service";
import { CustomSearch } from "_baseDirectory/types/custom-search.types";
import { checkFrequencyAndDistributionCriteria } from "_baseDirectory/validators/custom-search-frequency.validator";
import { SearchFilter } from "_baseDirectory/types/search-filter.types";
import Management_NameCacheService from "../management/cache/management-_name-cache.service";
import Management_NameCacheRepository
	from "../management/cache/management-_name-cache.repository";
import { QueueServiceExecutableInterface } from "_baseDirectory/external-references";
// import CreateOrUpdateVisitTimelineMicroservicePostQueueRepository
// 	from "../../../../core/visit-timeline-microservice-post/queue/create-or-update/create-or-update-visit-timeline-microservice-post-queue.repository";
import SavePictureDistribution_NameQueueRepository from "./queue/save-report/distribution-save-report-_name-queue.repository";

export default class Distribution_NameService
	extends CustomSearchService
	implements QueueServiceExecutableInterface<PrepareAndSendPictureProps> {
	executionRoutineRepository = new Distribution_NameDistributionRepository();
	typeConfigService = new TypeConfigService();

	constructor() {
		super(Distribution_NameDistributionRepository, CreateDistribution_NameValidator, UpdateDistribution_NameValidator, DeleteDistribution_NameValidator);

	}

	async exec(props: PrepareAndSendPictureProps): Promise<PrepareAndSendPictureProps> {
		if (props.file) {
			props.file.buffer = Buffer.from(props.file.buffer.data, "utf-8");
			const picture = await this.sendPicture({
				data: props.data,
				file: props.file,
				visitId: props.visitId
			});

			if (picture) {
				const savePictureDistribution_NameRepository = new SavePictureDistribution_NameQueueRepository();
				await savePictureDistribution_NameRepository.save({
					...picture
				})
			}
		}

		// if (props.visitId) {
		// 	const createOrUpdateVisitTimelineMicroservicePostQueueRepository = new CreateOrUpdateVisitTimelineMicroservicePostQueueRepository();

		// 	await createOrUpdateVisitTimelineMicroservicePostQueueRepository.save({
		// 		visitId: props.visitId
		// 	});
		// }

		return props;
	}

	async index() {

		let response: any = {
			search_list: []
		};

		return response;
	}

	async read(requestFilters?: SearchFilter[]) {
		const executionRoutineCacheService = new Management_NameCacheService(
			new Management_NameCacheRepository()
		);

		const cacheSearch = await executionRoutineCacheService.getAll();

		if (cacheSearch.length) {
			return cacheSearch;
		}

		let response: any[] = [];

		const searchList: any[] = <any[]>await this.executionRoutineRepository
			.find({
				published: true
			})
			.sort({ publishedAt: -1 })
			.size(1)
			.exec();

		await Promise.all(searchList.map(async (custom_search: CustomSearch) => {

			let { search_setup, filters } = custom_search;

			const searchIsValid = checkFrequencyAndDistributionCriteria({
				search_setup: search_setup!,
				instanceFilters: filters!,
				requestFilters
			});

			if (!searchIsValid) {
				return;
			}

			const { blocks } = custom_search;

			for (let block of blocks) {

				let { search_setup, filters } = block;

				const blockIsValid = checkFrequencyAndDistributionCriteria({
					search_setup: search_setup!,
					instanceFilters: filters!,
					requestFilters
				});


				if (!blockIsValid) {
					custom_search.blocks.splice(custom_search.blocks.indexOf(block), 1);
					continue;
				}

				if (block.type == "list") {

					const type_config: CustomSearchConfigListInterface = block.type_config;

					block.collect_items = [];

					if (type_config && Object.keys(type_config).length) {

						switch (type_config.type) {
							case "normal":
								let collect_items = await this.typeConfigService.getProductsByRepository(type_config);

								for (let item of collect_items) {
									block.collect_items.push({
										...item,
										description: "",
										alerts: [],
										alert_description: "",
										type: "collect_item",
										finish: false,
										components: []
									});
								}
								break;
						}
					}
				}
			}

			if (custom_search) {
				response.push(custom_search);
			}
		}));

		await executionRoutineCacheService.save(JSON.stringify(response));

		return response;
	}
}