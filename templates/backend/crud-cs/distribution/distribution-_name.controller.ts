import { Request, Response } from "express";
import Distribution_NameService from "./distribution-_name.service";
import * as SearchUtils from "../../../utils/custom-search.utils";
import { CustomSearch } from "_baseDirectory/types/custom-search.types";
import Distribution_NamePictureQueueRepository from "./queue/pictures/distribution-_name-picture-queue.repository";
import Report_NameService from "../report/report-_name.service";

const getInstance = () => {

    return new Distribution_NameService();
}

export const index = async (req: Request, res: Response) => {

    const service = getInstance();

    let response = await service.index();

    res.send(response);

}

export const create = async (req: Request, res: Response) => {

    const service = getInstance();

    const { name, description } = req.body;

    const _personal_info = {

        name: "Usuário",
        last_name: "Teste"

    }

    let response: any = {};

    let searchModel = SearchUtils.getSearchModel({ name, description });

    searchModel.creator.displayName = _personal_info.name + " " + _personal_info.last_name;

    const created = await service.create(searchModel);

    response = SearchUtils.getDefaultKeys(searchModel);

    response._id = created.id;

    res.send(response);

}

export const save = async (req: Request, res: Response) => {

}

export const read = async (req: Request, res: Response) => {

    const service = getInstance();

    const { filters } = req.body;

    let response = await service.read(filters);

    res.send(response);

}

export const readCustomSearchForm = async (req: Request, res: Response) => {

    const service = getInstance();

    const { id } = req.params;

    let response = await service.readCustomSearchForm({ visitId: id });

    res.send(response);

}

export const readCustomSearchReport = async (req: Request, res: Response) => {

    const service = new Report_NameService();

    const { id } = req.params;

    let response = await service.read(id);

    res.send(response);
}

export const readFilters = async (req: Request, res: Response) => {


    const data = getInstance().readFilters();

    res.send(data);

}

export const customSearchListConfig = async (req: Request, res: Response) => {
    res.send({});
}

export const publish = async (req: Request, res: Response) => {

    const service = getInstance();

    const searchModel = <CustomSearch>req.body;

    const { visitId } = searchModel;

    let response = await service.publishForm(searchModel, { visitId: visitId });

    await service.publishHistory(searchModel, "_name")

    res.send(response);

}

export const sendPicture = async (req: Request, res: Response) => {
    const service = getInstance();
    const reportService = new ReportDistribution_NameService();

    const file = req.file;
    const data = req.body;

    const { visitId } = data;

    let response = await service.prepareAndSendPicture({
        data,
        file,
        visitId
    });

    if(response){
        await reportService.sendReport({ ...response });
        return res.send(response);
    }

    return res.send([]);
}

export const sendPictureV2 = async (req: Request, res: Response) => {
    const executionRoutinePictureQueueRepository = new Distribution_NamePictureQueueRepository();

    const file = req.file;
    const data = req.body;

    const { visitId } = data;

    await executionRoutinePictureQueueRepository.save({
        data,
        file,
        visitId
    }, {
        delay: 5000,
        attempts: 5,
    });

    return res.send([]);
}

export const putNewPicture = async (req: Request, res: Response) => {
    const service = new ReportDistribution_NameService();

    const data = req.body;

    const publishData = await service.sendReport({ ...data });

    res.send(publishData);


}