import { Schema, model } from "mongoose";
import { DefaultSchema, DefaultSchemaOptions } from "_baseDirectory/external-references";
import { CustomSearchSchema } from "_baseDirectory/schemas/custom-search.schema";


const Distribution_NameSchema = new Schema({
    ...DefaultSchema,
    ...CustomSearchSchema,
    visitId: {
        type: Schema.Types.String,
        default: null
    },
    publishId: {
        type: Schema.Types.String,
        default: null
    },
    userId: {
        type: Schema.Types.ObjectId,
        default: null
    },
    clientId: {
        type: Schema.Types.ObjectId,
        default: null
    }

}, DefaultSchemaOptions)


const Distribution_NameModel = model("Distribution_NameForm", Distribution_NameSchema);

export default Distribution_NameModel;