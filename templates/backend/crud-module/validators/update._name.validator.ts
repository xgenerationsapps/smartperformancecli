import BaseValidator from "../../../module-base/base.validator";
import { I_Name } from "../_name.interface";

export default class Update_NameValidator extends BaseValidator<I_Name> {
	constructor(data: any) {
		super(data, {
			name: ["required", `uniqueUpdateRule:_namespace,${data.id}`],
		});
	}
}