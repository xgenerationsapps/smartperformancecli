import BaseValidator from "../../../module-base/base.validator";
import { I_Name } from "../_name.interface";

export default class Create_NameValidator extends BaseValidator<I_Name> {
	constructor(data: any) {
		super(data, {
			name: ["required", "unique:_namespace"],
		});
	}
}