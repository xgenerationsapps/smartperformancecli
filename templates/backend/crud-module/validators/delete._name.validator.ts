import BaseValidator from "../../../module-base/base.validator";
import { I_Name } from "../_name.interface";

export default class Delete_NameValidator extends BaseValidator<I_Name> {
	constructor(data: any) {
		super(data, {
			id: ["required", "exists:_namespace"],
		});
	}
}