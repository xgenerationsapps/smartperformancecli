import { Schema } from "mongoose";
import { DefaultSchema, DefaultSchemaOptions } from "../../module-base/repositories/mongoose/default.schema";

const _NameSchema = new Schema({
	...DefaultSchema,
	name: {
		type: String,
		required: true,
	}
}, DefaultSchemaOptions);

export default _NameSchema;