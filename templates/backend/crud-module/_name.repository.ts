import { I_Name } from "./_name.interface";
import BaseModuleRepository from "../../module-base/base.repository";
import _NameModel from "./_name.model";

export default class _NameRepository extends BaseModuleRepository<I_Name> {
	constructor() {
		super(_NameModel);
	}
}