import { Router } from "express";
import { PermissionByTagMiddleware } from "../../module-backend-node/module-core/permission-module/middlewares/permission.middleware";
import * as _NameController from "./_name.controller";

const _NameRoutes = Router();

_NameRoutes.get("/", PermissionByTagMiddleware("read_name"), _NameController.index);
_NameRoutes.post("/", PermissionByTagMiddleware("create_name"), _NameController.create);
_NameRoutes.get("/:id", PermissionByTagMiddleware("read_name"), _NameController.findById);
_NameRoutes.put("/:id", PermissionByTagMiddleware("update_name"), _NameController.updateById);
_NameRoutes.delete("/:id", PermissionByTagMiddleware("delete_name"), _NameController.deleteById);

export default _NameRoutes;