import { WebServiceArgsType } from "@smart-performance-react-native/database/dist/base.types";
import { FastRequestService } from "@smart-performance-react-native/fast-request";
import { _NameInterface } from "./_name.interface";
export const _nameCallMethod: (props?: WebServiceArgsType) => Promise<_NameInterface[]> = async (props) => {
    return new FastRequestService()
        .simpleRequestWithAuth({
            /**@param { { url : string, type: "post" | "get" }} route */
            route: {} as any,
            query: props?.query
        })
}//rename async () => { return } to your http api