
import { DefaultSchema } from "@smart-performance-react-native/database/dist/default.schema";

export const _NameSchema = {
    name: "_Name",
    properties: {
        ...DefaultSchema.properties
    }
}