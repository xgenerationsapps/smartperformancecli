package _CorePackageName;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface _NameInterface {

    @GET("")
    Call<_NameResponse> index();

    @POST("")
    Call<_NameResponse> create(@Body _NameBody body);
}
