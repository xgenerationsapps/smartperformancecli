import { BaseControllerV2 } from "@smart-performance-react-native/database";
import { _NameInterface } from "./_name.interface";
import { _NameSchema } from "./_name.schema";
import { _NameService } from "./_name.service";

export class _NameController extends BaseControllerV2<_NameInterface> {

    service: _NameService;
    constructor() {
        super()
        this.service = new _NameService({
            path: "_namePath",
            name: "_Name",
            schema: [
                _NameSchema
            ],
            schemaVersion: 0
        });
    }

}