package _CorePackageName;

import _BasePackageName._BaseTag.BaseController;
import _CorePackageName._NameSchema;

public class _NameController extends BaseController<_NameSchema> {

    private final String name = "_name";
    private final int version = 0;

    public _NameController() {
        this.service = new _NameService(this.name, _NameSchema.class, this.version);
    }

    @Override
    public _NameController getInstance() {

        super.getInstance();

        return this;
    }


}
