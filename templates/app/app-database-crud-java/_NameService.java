package _CorePackageName;

import android.annotation.SuppressLint;
import android.util.Log;

import org.bson.types.ObjectId;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import io.realm.RealmList;
import _BasePackageName._BaseTag.BaseService;
import _CorePackageName._NameSchema;

public class _NameService extends BaseService<_NameSchema> {

    public _NameService(String name, Class<_NameSchema> schema, int version) {
        this.name = name;
        this.schema = schema;
        this.version = version;
    }

}
