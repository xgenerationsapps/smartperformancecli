package _CorePackageName;

import org.bson.types.ObjectId;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class _NameSchema extends RealmObject {

    @PrimaryKey
    private ObjectId id = new ObjectId();

    
    public _NameSchema() {

        
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

}
