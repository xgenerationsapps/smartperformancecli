export type _NameTypeNames =
    "@_Name/READALL" |
    "@_Name/READONE" |
    "@_Name/UPDATEONE" |
    "@_Name/DELETEONE" |
    "@_Name/CLEAR"

export enum _NameTypes {
    READALL = "@_Name/READALL",
    READONE = "@_Name/READONE",
    UPDATEONE = "@_Name/UPDATEONE",
    DELETEONE = "@_Name/DELETEONE",
    CLEAR = "@_Name/CLEAR",
}