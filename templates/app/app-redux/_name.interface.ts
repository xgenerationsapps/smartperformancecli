import { _NameTypeNames } from "./_name.types"

export interface I_NameAction {
    type: _NameTypeNames,
    data: Partial<I_NameData>
}

export interface I_NameData {
    id: string
}