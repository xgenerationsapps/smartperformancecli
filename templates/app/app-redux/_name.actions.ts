import { DefaultStore } from "@smart-performance-react-native/fast-request/dist/external-references/redux"
import { I_NameAction, I_NameData } from "./_name.interface"
import { _NameTypes } from "./_name.types"

export const readAll = (): I_NameData[] => {
    return (DefaultStore.getState()._NameReducer as I_NameData[])
}

export const readOne = (id: string): I_NameData | undefined => {
    return (DefaultStore.getState()._NameReducer as I_NameData[]).find(element => element.id == id);
}

export const updateOne = (id: string, props: Partial<I_NameData>) => {

    const set = (id: string, props: Partial<I_NameData>): I_NameAction => {
        return {
            type: _NameTypes.UPDATEONE,
            data: Object.assign({ id }, props)
        }
    }

    DefaultStore.dispatch(set(id, props));
}

export const deleteOne = (id: string) => {

    const set = (id: string): Partial<I_NameAction> => {
        return {
            type: _NameTypes.DELETEONE,
            data: { id }
        }
    }

    DefaultStore.dispatch(set(id));
}

export const clear = () => {
    const set = (): Partial<I_NameAction> => {
        return {
            type: _NameTypes.CLEAR,
        }
    }

    DefaultStore.dispatch(set());
}