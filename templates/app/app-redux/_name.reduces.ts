import { _NameInitialState } from './_name.initial-state';
import { I_NameAction } from './_name.interface';

export const _NameReducer = (state = _NameInitialState, action: I_NameAction) => {

    const { type, data } = action;
    const { id } = data;


    switch (type) {
        case "@_Name/READALL":
            return state;
        case "@_Name/READONE":
            return state;
        case "@_Name/UPDATEONE":
            if (id) {

                const c_Name = state.find(element => element.id == id);

                if (c_Name) {
                    const update_NameIndex = state.indexOf(c_Name);

                    state[update_NameIndex] = Object.assign(c_Name, data);
                }
            }
            return state;
        case "@_Name/DELETEONE":
            return state.filter(element => element.id != id);
        case "@_Name/CLEAR":
            state = [];
            return state;
        default:
            return state;
    }
}