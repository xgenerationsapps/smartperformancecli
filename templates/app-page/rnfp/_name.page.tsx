import React from 'react'
import { View } from 'react-native';
import { _NameParams, _NameProps } from './_name.interface';
import { _NameStyles } from './_name.styles';
import { _NameService } from './_name.service';
import { BaseStyles } from '_baseDirectory/base.styles'
import { NavigationProps } from '_baseDirectory/types/navigation.interface'
import { ReactProps } from '_baseDirectory/types/react.interface'

type PageProps = _NameProps & ReactProps & NavigationProps<_NameParams>

export default function _NamePage(props: PageProps) {

    return (
        <View style={BaseStyles.container}>
        </View>
    )
}
