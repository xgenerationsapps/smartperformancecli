import _NamePage from "./_name.page";
import { _NameProps } from "./_name.interface";

export {
    _NamePage,
    _NameProps
}