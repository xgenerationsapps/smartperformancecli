export interface Feature {
    show?: boolean
    align?: string,
    buttons?: {
        edit?: boolean,
        copy?: boolean,
        delete?: boolean
    }
}
