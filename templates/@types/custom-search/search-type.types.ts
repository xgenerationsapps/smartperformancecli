export interface SearchType {
    type: string,
    description: string
}