export interface Alert{
    color:string,
    value:string,
    alert_id:string,
    active:boolean
}