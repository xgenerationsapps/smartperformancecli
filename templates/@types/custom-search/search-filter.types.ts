export interface SearchFilter {
    name: string,
    filter_list: string[]
}