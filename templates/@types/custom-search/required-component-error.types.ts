export interface RequiredComponentError { 
    title: string, 
    message: string
 }