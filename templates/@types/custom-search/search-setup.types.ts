export interface SearchSetup {
    frequency_group: string,
    frequency_type: string,
    init_date: number,
    end_date: number,
    never_expires: boolean,
    limit: number,
    version:string,
    
}