export interface ConditionalPointsComponent{
    type:string,
    okay_points:number,
    not_okay_points:number
}