export interface Slide {
    images: any[],
    unique_key: "",
    document_name: any,
    name: "",
    channel: "",
    region: ""
};