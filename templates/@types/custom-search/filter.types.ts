export interface Filter {
    name: string,
    value_type: "value" | "id"
}