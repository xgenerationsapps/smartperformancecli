import { TableData } from "./table-data.types"

export interface DefaultData {
    headers: string[],
    data: Array<Array<TableData>>
}