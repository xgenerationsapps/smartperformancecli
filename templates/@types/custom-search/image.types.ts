export interface Image{
    component_id: string,
    file?: string,
    url?: string
}