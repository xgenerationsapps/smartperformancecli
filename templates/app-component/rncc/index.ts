import _NameComponent from "./_name.component";
import { _NameProps } from "./_name.interface";

export {
    _NameComponent,
    _NameProps
}