import React, { Component } from 'react'
import { View } from 'react-native'
import { _NameProps, State } from './_name.interface'
import { _NameStyles } from './_name.styles'
import { ReactProps } from '_baseDirectory/types/react.interface'

type ComponentProps = _NameProps & ReactProps
export default class _NameComponent extends Component<ComponentProps, State> {

  constructor(props: ComponentProps) {
    super(props)

    this.state = {

    }
  }


  render() {
    return (
      <View></View>
    )
  }
}
