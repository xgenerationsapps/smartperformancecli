import React from 'react'
import { View } from 'react-native';
import { _NameProps } from './_name.interface';
import { _NameStyles } from './_name.styles';
import { ReactProps } from '_baseDirectory/types/react.interface'

type ComponentProps = _NameProps & ReactProps

export default function _NameComponent(props: ComponentProps) {

    return (
        <View>
        </View>
    )
}
