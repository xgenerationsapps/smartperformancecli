import { getBaseDirectoryConfig } from "./base-directory.utils";
import { promisify } from 'util';
import fs, { exists } from 'fs';
import path from 'path';

const stat = promisify(fs.stat);
const readFile = promisify(fs.readFile);
const readDir = promisify(fs.readdir);

const BaseConfig = {
    searchInDirectory: "",
    type: "directory",
    includedFiles: [],
    ignoredFiles: []
}
export const searchFileOrDirectory = async (config = BaseConfig) => {

    let { searchInDirectory, type, includedFiles, ignoredFiles } = config;

    if (!type) {
        type = "directory";
    }

    let response = {
        endPointTag: "",
        fileSearched: false,
        relativePath: "",
        fullPath: "",
        data: undefined
    }

    let _searchInDirectory = searchInDirectory;

    while (!response.fileSearched) {
        const statResponse = await stat(path.resolve(_searchInDirectory));

        let pathSplit = _searchInDirectory.split("\\")
        if (!pathSplit.length) {
            pathSplit = _searchInDirectory.split("/")
        }

        if (pathSplit.length == 1 || pathSplit.length == 2 && pathSplit[1] == '') {

            break;
        }

        if (statResponse.isDirectory()) {
            const filesInDirectory = await readDir(_searchInDirectory);

            if (type == "directory") {
                const _baseDirectoryConfig = getBaseDirectoryConfig(filesInDirectory, includedFiles);

                const ignoredExists = filesInDirectory.find((file) => ignoredFiles.includes(file))

                if (_baseDirectoryConfig.baseTag && !ignoredExists) {
                    response.fileSearched = true;
                    response.fullPath = _searchInDirectory;
                    response.endPointTag = _baseDirectoryConfig.baseTag;
                }



            } else if (type == "file") {

                const includedFile = filesInDirectory.find((file) => includedFiles.includes(file))

                if (includedFile) {

                    response.fileSearched = true;
                    const data = await readFile(path.resolve(_searchInDirectory, includedFile), 'utf8');;

                    if (data)
                        response.data = JSON.parse(data)
                }
            }

            // console.log("is directory:", _searchInDirectory);
            response.relativePath += "../"

            if (!response.fileSearched) {
                // console.log("base not searched in:", _searchInDirectory);
                _searchInDirectory = path.resolve(_searchInDirectory, "../");
                // console.log("up paste:", _searchInDirectory);
            } else {
                break;
            }

        } else {
            // console.log("is not directory:", _searchInDirectory);
            _searchInDirectory = path.resolve(_searchInDirectory, "../");
            // console.log("up paste:", response.relativePath);
        }


    }

    response.relativePath = response.relativePath.substring(0, response.relativePath.length - 3);

    return response;

}